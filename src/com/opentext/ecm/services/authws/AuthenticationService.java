
package com.opentext.ecm.services.authws;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.4-b01 Generated
 * source version: 2.2
 * 
 */

@WebServiceClient(name = "Authentication", targetNamespace = "urn:Core.service.livelink.opentext.com", wsdlLocation = "http://localhost:8080/cws/services/Authentication?wsdl")
public class AuthenticationService extends Service {
	private final static URL AUTHENTICATIONSERVICE_WSDL_LOCATION;
	private final static WebServiceException AUTHENTICATIONSERVICE_EXCEPTION;
	// private final static QName AUTHENTICATIONSERVICE_QNAME = new
	// QName("urn:authws.services.ecm.opentext.com",
	// "AuthenticationService");
	private final static QName AUTHENTICATIONSERVICE_QNAME = new QName("urn:Core.service.livelink.opentext.com",
			"Authentication");

	static {
		// String[] arr = null;
		// InetAddress ip;
		// String hostname;
		// try {
		// ip = InetAddress.getLocalHost();
		// String r = ip.toString();
		// arr = r.split("/");
		// } catch (UnknownHostException e) {
		// e.printStackTrace();
		// }

		URL url = null;
		WebServiceException e = null;
		try {
			url = new URL("http://localhost:8080/cws/services/Authentication?wsdl");
		} catch (MalformedURLException ex) {
			e = new WebServiceException(ex);
		}
		AUTHENTICATIONSERVICE_WSDL_LOCATION = url;
		AUTHENTICATIONSERVICE_EXCEPTION = e;
	}

	public AuthenticationService() {
		super(__getWsdlLocation(), AUTHENTICATIONSERVICE_QNAME);
	}

	public AuthenticationService(WebServiceFeature... features) {
		super(__getWsdlLocation(), AUTHENTICATIONSERVICE_QNAME, features);
	}

	public AuthenticationService(URL wsdlLocation) {
		super(wsdlLocation, AUTHENTICATIONSERVICE_QNAME);
	}

	public AuthenticationService(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, AUTHENTICATIONSERVICE_QNAME, features);
	}

	public AuthenticationService(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public AuthenticationService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 * 
	 * @return returns Authentication
	 */
	@WebEndpoint(name = "AuthenticationPort")
	public Authentication getAuthenticationPort() {
		return super.getPort(new QName("urn:Core.service.livelink.opentext.com", "AuthenticationPort"),
				Authentication.class);
	}

	/**
	 * 
	 * @param features
	 *            A list of {@link javax.xml.ws.WebServiceFeature} to configure on
	 *            the proxy. Supported features not in the <code>features</code>
	 *            parameter will have their default values.
	 * @return returns Authentication
	 */
	@WebEndpoint(name = "AuthenticationPort")
	public Authentication getAuthenticationPort(WebServiceFeature... features) {
		return super.getPort(new QName("urn:Core.service.livelink.opentext.com", "AuthenticationPort"),
				Authentication.class, features);
	}

	private static URL __getWsdlLocation() {
		if (AUTHENTICATIONSERVICE_EXCEPTION != null) {
			throw AUTHENTICATIONSERVICE_EXCEPTION;
		}
		return AUTHENTICATIONSERVICE_WSDL_LOCATION;
	}

}
