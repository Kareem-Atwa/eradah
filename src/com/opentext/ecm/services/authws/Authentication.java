
package com.opentext.ecm.services.authws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "Authentication", targetNamespace = "urn:authws.services.ecm.opentext.com")
@XmlSeeAlso({
    com.opentext.ecm.services.authws.ObjectFactory.class,
    com.opentext.ecm.api.ObjectFactory.class
})
public interface Authentication {


    /**
     * 
     * @param username
     * @param password
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "Authenticate")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "Authenticate", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.Authenticate")
    @ResponseWrapper(localName = "AuthenticateResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:Authenticate:Fault:AuthenticationException")
    })
    public String authenticate(
        @WebParam(name = "username", targetNamespace = "")
        String username,
        @WebParam(name = "password", targetNamespace = "")
        String password)
        throws AuthenticationException_Exception
    ;

    /**
     * 
     * @param username
     * @param code
     * @param password
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "AuthenticateWithPasswordAndCode")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "AuthenticateWithPasswordAndCode", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateWithPasswordAndCode")
    @ResponseWrapper(localName = "AuthenticateWithPasswordAndCodeResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateWithPasswordAndCodeResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithPasswordAndCodeRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithPasswordAndCodeResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithPasswordAndCode:Fault:AuthenticationException")
    })
    public String authenticateWithPasswordAndCode(
        @WebParam(name = "username", targetNamespace = "")
        String username,
        @WebParam(name = "password", targetNamespace = "")
        String password,
        @WebParam(name = "code", targetNamespace = "")
        String code)
        throws AuthenticationException_Exception
    ;

    /**
     * 
     * @param username
     * @param token
     * @param code
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "AuthenticateWithTokenAndCode")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "AuthenticateWithTokenAndCode", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateWithTokenAndCode")
    @ResponseWrapper(localName = "AuthenticateWithTokenAndCodeResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateWithTokenAndCodeResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithTokenAndCodeRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithTokenAndCodeResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithTokenAndCode:Fault:AuthenticationException")
    })
    public String authenticateWithTokenAndCode(
        @WebParam(name = "username", targetNamespace = "")
        String username,
        @WebParam(name = "token", targetNamespace = "")
        byte[] token,
        @WebParam(name = "code", targetNamespace = "")
        String code)
        throws AuthenticationException_Exception
    ;

    /**
     * 
     * @param username
     * @param token
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "AuthenticateWithToken")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "AuthenticateWithToken", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateWithToken")
    @ResponseWrapper(localName = "AuthenticateWithTokenResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateWithTokenResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithTokenRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithTokenResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateWithToken:Fault:AuthenticationException")
    })
    public String authenticateWithToken(
        @WebParam(name = "username", targetNamespace = "")
        String username,
        @WebParam(name = "token", targetNamespace = "")
        byte[] token)
        throws AuthenticationException_Exception
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "AuthenticateCurrentUser")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "AuthenticateCurrentUser", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateCurrentUser")
    @ResponseWrapper(localName = "AuthenticateCurrentUserResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateCurrentUserResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateCurrentUserRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateCurrentUserResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateCurrentUser:Fault:AuthenticationException")
    })
    public String authenticateCurrentUser()
        throws AuthenticationException_Exception
    ;

    /**
     * 
     * @param code
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "AuthenticateCurrentUserWithCode")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "AuthenticateCurrentUserWithCode", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateCurrentUserWithCode")
    @ResponseWrapper(localName = "AuthenticateCurrentUserWithCodeResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.AuthenticateCurrentUserWithCodeResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateCurrentUserWithCodeRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateCurrentUserWithCodeResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:AuthenticateCurrentUserWithCode:Fault:AuthenticationException")
    })
    public String authenticateCurrentUserWithCode(
        @WebParam(name = "code", targetNamespace = "")
        String code)
        throws AuthenticationException_Exception
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "GetResourceId")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "GetResourceId", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.GetResourceId")
    @ResponseWrapper(localName = "GetResourceIdResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.GetResourceIdResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:GetResourceIdRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:GetResourceIdResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:GetResourceId:Fault:AuthenticationException")
    })
    public String getResourceId()
        throws AuthenticationException_Exception
    ;

    /**
     * 
     * @param resourceId
     * @param username
     * @return
     *     returns java.lang.String
     * @throws AuthenticationException_Exception
     */
    @WebMethod(operationName = "GetTicketForUser")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "GetTicketForUser", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.GetTicketForUser")
    @ResponseWrapper(localName = "GetTicketForUserResponse", targetNamespace = "urn:authws.services.ecm.opentext.com", className = "com.opentext.ecm.services.authws.GetTicketForUserResponse")
    @Action(input = "urn:authws.services.ecm.opentext.com:Authentication:GetTicketForUserRequest", output = "urn:authws.services.ecm.opentext.com:Authentication:GetTicketForUserResponse", fault = {
        @FaultAction(className = AuthenticationException_Exception.class, value = "urn:authws.services.ecm.opentext.com:Authentication:GetTicketForUser:Fault:AuthenticationException")
    })
    public String getTicketForUser(
        @WebParam(name = "username", targetNamespace = "")
        String username,
        @WebParam(name = "resourceId", targetNamespace = "")
        String resourceId)
        throws AuthenticationException_Exception
    ;

}
