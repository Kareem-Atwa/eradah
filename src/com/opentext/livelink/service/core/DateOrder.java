
package com.opentext.livelink.service.core;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DateOrder.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DateOrder">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DDMMYY"/>
 *     &lt;enumeration value="DDYYMM"/>
 *     &lt;enumeration value="MMDDYY"/>
 *     &lt;enumeration value="MMYYDD"/>
 *     &lt;enumeration value="YYDDMM"/>
 *     &lt;enumeration value="YYMMDD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DateOrder")
@XmlEnum
public enum DateOrder {

    DDMMYY,
    DDYYMM,
    MMDDYY,
    MMYYDD,
    YYDDMM,
    YYMMDD;

    public String value() {
        return name();
    }

    public static DateOrder fromValue(String v) {
        return valueOf(v);
    }

}
