
package com.opentext.livelink.service.core;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MonthFormat.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MonthFormat">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FULLNAME"/>
 *     &lt;enumeration value="THREECHAR"/>
 *     &lt;enumeration value="TWODIGIT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MonthFormat")
@XmlEnum
public enum MonthFormat {

    FULLNAME,
    THREECHAR,
    TWODIGIT;

    public String value() {
        return name();
    }

    public static MonthFormat fromValue(String v) {
        return valueOf(v);
    }

}
