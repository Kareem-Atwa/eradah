
package com.opentext.livelink.service.memberservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateMemberResult" type="{urn:MemberService.service.livelink.opentext.com}Member" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createMemberResult"
})
@XmlRootElement(name = "CreateMemberResponse")
public class CreateMemberResponse {

    @XmlElement(name = "CreateMemberResult")
    protected Member createMemberResult;

    /**
     * Gets the value of the createMemberResult property.
     * 
     * @return
     *     possible object is
     *     {@link Member }
     *     
     */
    public Member getCreateMemberResult() {
        return createMemberResult;
    }

    /**
     * Sets the value of the createMemberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Member }
     *     
     */
    public void setCreateMemberResult(Member value) {
        this.createMemberResult = value;
    }

}
