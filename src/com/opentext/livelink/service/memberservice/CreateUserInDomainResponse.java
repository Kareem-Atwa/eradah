
package com.opentext.livelink.service.memberservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateUserInDomainResult" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createUserInDomainResult"
})
@XmlRootElement(name = "CreateUserInDomainResponse")
public class CreateUserInDomainResponse {

    @XmlElement(name = "CreateUserInDomainResult")
    protected long createUserInDomainResult;

    /**
     * Gets the value of the createUserInDomainResult property.
     * 
     */
    public long getCreateUserInDomainResult() {
        return createUserInDomainResult;
    }

    /**
     * Sets the value of the createUserInDomainResult property.
     * 
     */
    public void setCreateUserInDomainResult(long value) {
        this.createUserInDomainResult = value;
    }

}
