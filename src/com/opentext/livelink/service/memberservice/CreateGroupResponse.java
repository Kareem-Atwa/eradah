
package com.opentext.livelink.service.memberservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateGroupResult" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createGroupResult"
})
@XmlRootElement(name = "CreateGroupResponse")
public class CreateGroupResponse {

    @XmlElement(name = "CreateGroupResult")
    protected long createGroupResult;

    /**
     * Gets the value of the createGroupResult property.
     * 
     */
    public long getCreateGroupResult() {
        return createGroupResult;
    }

    /**
     * Sets the value of the createGroupResult property.
     * 
     */
    public void setCreateGroupResult(long value) {
        this.createGroupResult = value;
    }

}
