
package com.opentext.livelink.service.collaboration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taskListInfo" type="{urn:Collaboration.service.livelink.opentext.com}TaskListInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taskListInfo"
})
@XmlRootElement(name = "CreateTaskList")
public class CreateTaskList {

    protected TaskListInfo taskListInfo;

    /**
     * Gets the value of the taskListInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TaskListInfo }
     *     
     */
    public TaskListInfo getTaskListInfo() {
        return taskListInfo;
    }

    /**
     * Sets the value of the taskListInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskListInfo }
     *     
     */
    public void setTaskListInfo(TaskListInfo value) {
        this.taskListInfo = value;
    }

}
