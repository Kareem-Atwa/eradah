
package com.opentext.livelink.service.collaboration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNewsResult" type="{urn:Collaboration.service.livelink.opentext.com}NewsInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getNewsResult"
})
@XmlRootElement(name = "GetNewsResponse")
public class GetNewsResponse {

    @XmlElement(name = "GetNewsResult")
    protected NewsInfo getNewsResult;

    /**
     * Gets the value of the getNewsResult property.
     * 
     * @return
     *     possible object is
     *     {@link NewsInfo }
     *     
     */
    public NewsInfo getGetNewsResult() {
        return getNewsResult;
    }

    /**
     * Sets the value of the getNewsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link NewsInfo }
     *     
     */
    public void setGetNewsResult(NewsInfo value) {
        this.getNewsResult = value;
    }

}
