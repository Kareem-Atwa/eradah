
package com.opentext.livelink.service.collaboration;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaskStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="COMPLETED"/>
 *     &lt;enumeration value="INPROCESS"/>
 *     &lt;enumeration value="ISSUE"/>
 *     &lt;enumeration value="ONHOLD"/>
 *     &lt;enumeration value="PENDING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TaskStatus")
@XmlEnum
public enum TaskStatus {

    CANCELLED,
    COMPLETED,
    INPROCESS,
    ISSUE,
    ONHOLD,
    PENDING;

    public String value() {
        return name();
    }

    public static TaskStatus fromValue(String v) {
        return valueOf(v);
    }

}
