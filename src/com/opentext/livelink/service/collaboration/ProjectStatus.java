
package com.opentext.livelink.service.collaboration;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProjectStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProjectStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CAUTION"/>
 *     &lt;enumeration value="CRITICAL"/>
 *     &lt;enumeration value="ONTARGET"/>
 *     &lt;enumeration value="PENDING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProjectStatus")
@XmlEnum
public enum ProjectStatus {

    CAUTION,
    CRITICAL,
    ONTARGET,
    PENDING;

    public String value() {
        return name();
    }

    public static ProjectStatus fromValue(String v) {
        return valueOf(v);
    }

}
