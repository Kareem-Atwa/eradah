
package com.opentext.livelink.service.collaboration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTaskListResult" type="{urn:Collaboration.service.livelink.opentext.com}TaskListInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTaskListResult"
})
@XmlRootElement(name = "GetTaskListResponse")
public class GetTaskListResponse {

    @XmlElement(name = "GetTaskListResult")
    protected TaskListInfo getTaskListResult;

    /**
     * Gets the value of the getTaskListResult property.
     * 
     * @return
     *     possible object is
     *     {@link TaskListInfo }
     *     
     */
    public TaskListInfo getGetTaskListResult() {
        return getTaskListResult;
    }

    /**
     * Sets the value of the getTaskListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskListInfo }
     *     
     */
    public void setGetTaskListResult(TaskListInfo value) {
        this.getTaskListResult = value;
    }

}
