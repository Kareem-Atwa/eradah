
package com.opentext.livelink.service.collaboration;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.opentext.livelink.service.collaboration package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.opentext.livelink.service.collaboration
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTaskGroupResponse }
     * 
     */
    public CreateTaskGroupResponse createCreateTaskGroupResponse() {
        return new CreateTaskGroupResponse();
    }

    /**
     * Create an instance of {@link GetUnreadInfoResponse }
     * 
     */
    public GetUnreadInfoResponse createGetUnreadInfoResponse() {
        return new GetUnreadInfoResponse();
    }

    /**
     * Create an instance of {@link UnreadInfo }
     * 
     */
    public UnreadInfo createUnreadInfo() {
        return new UnreadInfo();
    }

    /**
     * Create an instance of {@link GetTaskListResponse }
     * 
     */
    public GetTaskListResponse createGetTaskListResponse() {
        return new GetTaskListResponse();
    }

    /**
     * Create an instance of {@link TaskListInfo }
     * 
     */
    public TaskListInfo createTaskListInfo() {
        return new TaskListInfo();
    }

    /**
     * Create an instance of {@link PostTopicResponse }
     * 
     */
    public PostTopicResponse createPostTopicResponse() {
        return new PostTopicResponse();
    }

    /**
     * Create an instance of {@link TopicInfo }
     * 
     */
    public TopicInfo createTopicInfo() {
        return new TopicInfo();
    }

    /**
     * Create an instance of {@link GetTaskGroupResponse }
     * 
     */
    public GetTaskGroupResponse createGetTaskGroupResponse() {
        return new GetTaskGroupResponse();
    }

    /**
     * Create an instance of {@link TaskGroupInfo }
     * 
     */
    public TaskGroupInfo createTaskGroupInfo() {
        return new TaskGroupInfo();
    }

    /**
     * Create an instance of {@link CreateProjectResponse }
     * 
     */
    public CreateProjectResponse createCreateProjectResponse() {
        return new CreateProjectResponse();
    }

    /**
     * Create an instance of {@link UpdateNewsResponse }
     * 
     */
    public UpdateNewsResponse createUpdateNewsResponse() {
        return new UpdateNewsResponse();
    }

    /**
     * Create an instance of {@link GetMilestone }
     * 
     */
    public GetMilestone createGetMilestone() {
        return new GetMilestone();
    }

    /**
     * Create an instance of {@link ListNews }
     * 
     */
    public ListNews createListNews() {
        return new ListNews();
    }

    /**
     * Create an instance of {@link GetReplyByNameResponse }
     * 
     */
    public GetReplyByNameResponse createGetReplyByNameResponse() {
        return new GetReplyByNameResponse();
    }

    /**
     * Create an instance of {@link ReplyInfo }
     * 
     */
    public ReplyInfo createReplyInfo() {
        return new ReplyInfo();
    }

    /**
     * Create an instance of {@link GetDiscussionResponse }
     * 
     */
    public GetDiscussionResponse createGetDiscussionResponse() {
        return new GetDiscussionResponse();
    }

    /**
     * Create an instance of {@link DiscussionInfo }
     * 
     */
    public DiscussionInfo createDiscussionInfo() {
        return new DiscussionInfo();
    }

    /**
     * Create an instance of {@link GetParticipants }
     * 
     */
    public GetParticipants createGetParticipants() {
        return new GetParticipants();
    }

    /**
     * Create an instance of {@link UpdateMilestoneResponse }
     * 
     */
    public UpdateMilestoneResponse createUpdateMilestoneResponse() {
        return new UpdateMilestoneResponse();
    }

    /**
     * Create an instance of {@link CreateTask }
     * 
     */
    public CreateTask createCreateTask() {
        return new CreateTask();
    }

    /**
     * Create an instance of {@link TaskInfo }
     * 
     */
    public TaskInfo createTaskInfo() {
        return new TaskInfo();
    }

    /**
     * Create an instance of {@link CreateMilestoneResponse }
     * 
     */
    public CreateMilestoneResponse createCreateMilestoneResponse() {
        return new CreateMilestoneResponse();
    }

    /**
     * Create an instance of {@link ListTopicsResponse }
     * 
     */
    public ListTopicsResponse createListTopicsResponse() {
        return new ListTopicsResponse();
    }

    /**
     * Create an instance of {@link CreateNewsResponse }
     * 
     */
    public CreateNewsResponse createCreateNewsResponse() {
        return new CreateNewsResponse();
    }

    /**
     * Create an instance of {@link GetMyProjectsResponse }
     * 
     */
    public GetMyProjectsResponse createGetMyProjectsResponse() {
        return new GetMyProjectsResponse();
    }

    /**
     * Create an instance of {@link ProjectInfo }
     * 
     */
    public ProjectInfo createProjectInfo() {
        return new ProjectInfo();
    }

    /**
     * Create an instance of {@link ListRepliesResponse }
     * 
     */
    public ListRepliesResponse createListRepliesResponse() {
        return new ListRepliesResponse();
    }

    /**
     * Create an instance of {@link CreateTaskResponse }
     * 
     */
    public CreateTaskResponse createCreateTaskResponse() {
        return new CreateTaskResponse();
    }

    /**
     * Create an instance of {@link ListReplies }
     * 
     */
    public ListReplies createListReplies() {
        return new ListReplies();
    }

    /**
     * Create an instance of {@link GetReplyByName }
     * 
     */
    public GetReplyByName createGetReplyByName() {
        return new GetReplyByName();
    }

    /**
     * Create an instance of {@link UpdateTask }
     * 
     */
    public UpdateTask createUpdateTask() {
        return new UpdateTask();
    }

    /**
     * Create an instance of {@link GetMyChannels }
     * 
     */
    public GetMyChannels createGetMyChannels() {
        return new GetMyChannels();
    }

    /**
     * Create an instance of {@link UpdateTaskResponse }
     * 
     */
    public UpdateTaskResponse createUpdateTaskResponse() {
        return new UpdateTaskResponse();
    }

    /**
     * Create an instance of {@link CreateProject }
     * 
     */
    public CreateProject createCreateProject() {
        return new CreateProject();
    }

    /**
     * Create an instance of {@link GetMyAssignmentsResponse }
     * 
     */
    public GetMyAssignmentsResponse createGetMyAssignmentsResponse() {
        return new GetMyAssignmentsResponse();
    }

    /**
     * Create an instance of {@link Assignment }
     * 
     */
    public Assignment createAssignment() {
        return new Assignment();
    }

    /**
     * Create an instance of {@link ListTaskGroupsResponse }
     * 
     */
    public ListTaskGroupsResponse createListTaskGroupsResponse() {
        return new ListTaskGroupsResponse();
    }

    /**
     * Create an instance of {@link GetReadListResponse }
     * 
     */
    public GetReadListResponse createGetReadListResponse() {
        return new GetReadListResponse();
    }

    /**
     * Create an instance of {@link DiscussionReadList }
     * 
     */
    public DiscussionReadList createDiscussionReadList() {
        return new DiscussionReadList();
    }

    /**
     * Create an instance of {@link CreateTaskListResponse }
     * 
     */
    public CreateTaskListResponse createCreateTaskListResponse() {
        return new CreateTaskListResponse();
    }

    /**
     * Create an instance of {@link UpdateTaskGroup }
     * 
     */
    public UpdateTaskGroup createUpdateTaskGroup() {
        return new UpdateTaskGroup();
    }

    /**
     * Create an instance of {@link GetMilestoneResponse }
     * 
     */
    public GetMilestoneResponse createGetMilestoneResponse() {
        return new GetMilestoneResponse();
    }

    /**
     * Create an instance of {@link MilestoneInfo }
     * 
     */
    public MilestoneInfo createMilestoneInfo() {
        return new MilestoneInfo();
    }

    /**
     * Create an instance of {@link ListMilestonesResponse }
     * 
     */
    public ListMilestonesResponse createListMilestonesResponse() {
        return new ListMilestonesResponse();
    }

    /**
     * Create an instance of {@link GetReadList }
     * 
     */
    public GetReadList createGetReadList() {
        return new GetReadList();
    }

    /**
     * Create an instance of {@link GetMyAssignments }
     * 
     */
    public GetMyAssignments createGetMyAssignments() {
        return new GetMyAssignments();
    }

    /**
     * Create an instance of {@link UpdateNews }
     * 
     */
    public UpdateNews createUpdateNews() {
        return new UpdateNews();
    }

    /**
     * Create an instance of {@link NewsInfo }
     * 
     */
    public NewsInfo createNewsInfo() {
        return new NewsInfo();
    }

    /**
     * Create an instance of {@link ListMilestones }
     * 
     */
    public ListMilestones createListMilestones() {
        return new ListMilestones();
    }

    /**
     * Create an instance of {@link UpdateProjectParticipants }
     * 
     */
    public UpdateProjectParticipants createUpdateProjectParticipants() {
        return new UpdateProjectParticipants();
    }

    /**
     * Create an instance of {@link ProjectRoleUpdateInfo }
     * 
     */
    public ProjectRoleUpdateInfo createProjectRoleUpdateInfo() {
        return new ProjectRoleUpdateInfo();
    }

    /**
     * Create an instance of {@link UpdateProjectParticipantsResponse }
     * 
     */
    public UpdateProjectParticipantsResponse createUpdateProjectParticipantsResponse() {
        return new UpdateProjectParticipantsResponse();
    }

    /**
     * Create an instance of {@link CreateDiscussion }
     * 
     */
    public CreateDiscussion createCreateDiscussion() {
        return new CreateDiscussion();
    }

    /**
     * Create an instance of {@link ListNewsResponse }
     * 
     */
    public ListNewsResponse createListNewsResponse() {
        return new ListNewsResponse();
    }

    /**
     * Create an instance of {@link GetMyProjects }
     * 
     */
    public GetMyProjects createGetMyProjects() {
        return new GetMyProjects();
    }

    /**
     * Create an instance of {@link GetParticipantsResponse }
     * 
     */
    public GetParticipantsResponse createGetParticipantsResponse() {
        return new GetParticipantsResponse();
    }

    /**
     * Create an instance of {@link ProjectParticipants }
     * 
     */
    public ProjectParticipants createProjectParticipants() {
        return new ProjectParticipants();
    }

    /**
     * Create an instance of {@link GetMyTaskLists }
     * 
     */
    public GetMyTaskLists createGetMyTaskLists() {
        return new GetMyTaskLists();
    }

    /**
     * Create an instance of {@link CreateChannel }
     * 
     */
    public CreateChannel createCreateChannel() {
        return new CreateChannel();
    }

    /**
     * Create an instance of {@link ChannelInfo }
     * 
     */
    public ChannelInfo createChannelInfo() {
        return new ChannelInfo();
    }

    /**
     * Create an instance of {@link PostReply }
     * 
     */
    public PostReply createPostReply() {
        return new PostReply();
    }

    /**
     * Create an instance of {@link PostTopic }
     * 
     */
    public PostTopic createPostTopic() {
        return new PostTopic();
    }

    /**
     * Create an instance of {@link GetTaskResponse }
     * 
     */
    public GetTaskResponse createGetTaskResponse() {
        return new GetTaskResponse();
    }

    /**
     * Create an instance of {@link GetMyDiscussions }
     * 
     */
    public GetMyDiscussions createGetMyDiscussions() {
        return new GetMyDiscussions();
    }

    /**
     * Create an instance of {@link ListTaskGroups }
     * 
     */
    public ListTaskGroups createListTaskGroups() {
        return new ListTaskGroups();
    }

    /**
     * Create an instance of {@link ListTopics }
     * 
     */
    public ListTopics createListTopics() {
        return new ListTopics();
    }

    /**
     * Create an instance of {@link UpdateProject }
     * 
     */
    public UpdateProject createUpdateProject() {
        return new UpdateProject();
    }

    /**
     * Create an instance of {@link GetDiscussion }
     * 
     */
    public GetDiscussion createGetDiscussion() {
        return new GetDiscussion();
    }

    /**
     * Create an instance of {@link GetMyDiscussionsResponse }
     * 
     */
    public GetMyDiscussionsResponse createGetMyDiscussionsResponse() {
        return new GetMyDiscussionsResponse();
    }

    /**
     * Create an instance of {@link PostReplyResponse }
     * 
     */
    public PostReplyResponse createPostReplyResponse() {
        return new PostReplyResponse();
    }

    /**
     * Create an instance of {@link CreateDiscussionResponse }
     * 
     */
    public CreateDiscussionResponse createCreateDiscussionResponse() {
        return new CreateDiscussionResponse();
    }

    /**
     * Create an instance of {@link SetReadList }
     * 
     */
    public SetReadList createSetReadList() {
        return new SetReadList();
    }

    /**
     * Create an instance of {@link GetTaskGroup }
     * 
     */
    public GetTaskGroup createGetTaskGroup() {
        return new GetTaskGroup();
    }

    /**
     * Create an instance of {@link GetTopicByName }
     * 
     */
    public GetTopicByName createGetTopicByName() {
        return new GetTopicByName();
    }

    /**
     * Create an instance of {@link UpdateMilestone }
     * 
     */
    public UpdateMilestone createUpdateMilestone() {
        return new UpdateMilestone();
    }

    /**
     * Create an instance of {@link UpdateTaskGroupResponse }
     * 
     */
    public UpdateTaskGroupResponse createUpdateTaskGroupResponse() {
        return new UpdateTaskGroupResponse();
    }

    /**
     * Create an instance of {@link GetTask }
     * 
     */
    public GetTask createGetTask() {
        return new GetTask();
    }

    /**
     * Create an instance of {@link GetTaskList }
     * 
     */
    public GetTaskList createGetTaskList() {
        return new GetTaskList();
    }

    /**
     * Create an instance of {@link CreateNews }
     * 
     */
    public CreateNews createCreateNews() {
        return new CreateNews();
    }

    /**
     * Create an instance of {@link CreateTaskList }
     * 
     */
    public CreateTaskList createCreateTaskList() {
        return new CreateTaskList();
    }

    /**
     * Create an instance of {@link ListTasks }
     * 
     */
    public ListTasks createListTasks() {
        return new ListTasks();
    }

    /**
     * Create an instance of {@link CreateChannelResponse }
     * 
     */
    public CreateChannelResponse createCreateChannelResponse() {
        return new CreateChannelResponse();
    }

    /**
     * Create an instance of {@link GetNews }
     * 
     */
    public GetNews createGetNews() {
        return new GetNews();
    }

    /**
     * Create an instance of {@link GetUnreadInfo }
     * 
     */
    public GetUnreadInfo createGetUnreadInfo() {
        return new GetUnreadInfo();
    }

    /**
     * Create an instance of {@link GetChannel }
     * 
     */
    public GetChannel createGetChannel() {
        return new GetChannel();
    }

    /**
     * Create an instance of {@link GetMyTaskListsResponse }
     * 
     */
    public GetMyTaskListsResponse createGetMyTaskListsResponse() {
        return new GetMyTaskListsResponse();
    }

    /**
     * Create an instance of {@link GetTopicReplyResponse }
     * 
     */
    public GetTopicReplyResponse createGetTopicReplyResponse() {
        return new GetTopicReplyResponse();
    }

    /**
     * Create an instance of {@link DiscussionItem }
     * 
     */
    public DiscussionItem createDiscussionItem() {
        return new DiscussionItem();
    }

    /**
     * Create an instance of {@link SetReadListResponse }
     * 
     */
    public SetReadListResponse createSetReadListResponse() {
        return new SetReadListResponse();
    }

    /**
     * Create an instance of {@link CreateTaskGroup }
     * 
     */
    public CreateTaskGroup createCreateTaskGroup() {
        return new CreateTaskGroup();
    }

    /**
     * Create an instance of {@link ListTasksResponse }
     * 
     */
    public ListTasksResponse createListTasksResponse() {
        return new ListTasksResponse();
    }

    /**
     * Create an instance of {@link GetProjectResponse }
     * 
     */
    public GetProjectResponse createGetProjectResponse() {
        return new GetProjectResponse();
    }

    /**
     * Create an instance of {@link GetChannelResponse }
     * 
     */
    public GetChannelResponse createGetChannelResponse() {
        return new GetChannelResponse();
    }

    /**
     * Create an instance of {@link GetNewsResponse }
     * 
     */
    public GetNewsResponse createGetNewsResponse() {
        return new GetNewsResponse();
    }

    /**
     * Create an instance of {@link UpdateProjectResponse }
     * 
     */
    public UpdateProjectResponse createUpdateProjectResponse() {
        return new UpdateProjectResponse();
    }

    /**
     * Create an instance of {@link GetTopicByNameResponse }
     * 
     */
    public GetTopicByNameResponse createGetTopicByNameResponse() {
        return new GetTopicByNameResponse();
    }

    /**
     * Create an instance of {@link GetMyChannelsResponse }
     * 
     */
    public GetMyChannelsResponse createGetMyChannelsResponse() {
        return new GetMyChannelsResponse();
    }

    /**
     * Create an instance of {@link CreateMilestone }
     * 
     */
    public CreateMilestone createCreateMilestone() {
        return new CreateMilestone();
    }

    /**
     * Create an instance of {@link GetProject }
     * 
     */
    public GetProject createGetProject() {
        return new GetProject();
    }

    /**
     * Create an instance of {@link GetTopicReply }
     * 
     */
    public GetTopicReply createGetTopicReply() {
        return new GetTopicReply();
    }

    /**
     * Create an instance of {@link CollaborationAttachment }
     * 
     */
    public CollaborationAttachment createCollaborationAttachment() {
        return new CollaborationAttachment();
    }

    /**
     * Create an instance of {@link TopicAttachment }
     * 
     */
    public TopicAttachment createTopicAttachment() {
        return new TopicAttachment();
    }

    /**
     * Create an instance of {@link NewsAttachment }
     * 
     */
    public NewsAttachment createNewsAttachment() {
        return new NewsAttachment();
    }

    /**
     * Create an instance of {@link TaskListItem }
     * 
     */
    public TaskListItem createTaskListItem() {
        return new TaskListItem();
    }

}
