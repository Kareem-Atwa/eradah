package com.asset.Eradah;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opentext.livelink.service.docman.Node;

@Path("/resource")
public class Main {

	@POST
	@Path("/CreateDocument")
	@Consumes({ "multipart/form-data" })
	public Response CreateDocument(@FormDataParam("file") InputStream input,
			@FormDataParam("file") FormDataContentDisposition fileDispositions,
			@FormDataParam("cat_name") String cat_name, @FormDataParam("cat_values") String cat_values,
			@FormDataParam("parent_path") String parent_path, @FormDataParam("username") String username,
			@FormDataParam("password") String password)
			throws IOException, NumberFormatException, DatatypeConfigurationException, ParseException,
			ParserConfigurationException, SAXException, InterruptedException, SQLException {

		if (input == null || cat_name == null || cat_name.equals("") || cat_name.equals(" ") || cat_values == null
				|| cat_values.equals("") || cat_values.equals(" ") || parent_path == null || parent_path.equals("")
				|| parent_path.equals(" ") || username == null || username.equals("") || username.equals(" ")
				|| password == null || password.equals("") || password.equals(" ")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();

		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);
		Fun f = new Fun();
		// f.logs("CreateDocument Request Start");
		System.out.println("CreateDocument Request Start");
		long startTime = System.nanoTime();

		f.URL();
		String[] arr = fileDispositions.getFileName().split("[.]");
		byte[] doc = null;
		doc = IOUtils.toByteArray(input);
		// ByteArrayOutputStream os = new ByteArrayOutputStream();
		// byte[] buffer = new byte[1024];
		// int len;
		// while ((len = input.read(buffer)) != -1) {
		// os.write(buffer, 0, len);
		// }
		// doc = os.toByteArray();
		String doc_name = fileDispositions.getFileName();

		byte ptext[] = doc_name.getBytes("ISO-8859-1");
		String value = new String(ptext, "UTF-8");

		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect )")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET,PUT,UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type,Accept,X-Requested-With").build();

		}

		String folder_id = f.check_path(parent_path, username, password);
		if (folder_id.equals("error")) {
			return Response.status(400).entity("400 Path Not Found").header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();

		}

		if (f.check_document_exist(folder_id, username, password, doc_name)) {
			return Response.status(500).entity("500 Document Already Exist")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();

		}
		// String cat_id = f.search_cat(cat_name);
		// String ticket = f.authenticate_rest(username, password);
		// String cat_id = f.search_api_cat_test(ticket, cat_name);
		// String cat_id = f.get_catogory_id_xml(cat_name);
		String cat_id = f.test_return(cat_name);
		if (cat_id.equals("error") || cat_id.equals("error no item found")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();
		}
		String object_id = f.get_id(cat_id, doc, cat_values, parent_path, folder_id, value, arr[1]);

		if (object_id.equals("501")) {
			return Response.status(500).entity("500 Error Uploading Document")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		if (object_id.equals("700")) {
			return Response.status(501)
					.entity("501 Lookup Attribute Value�s is not found in the lookup table Or Wrong Value")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		if (object_id.equals("error")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		if (object_id.equals("value error")) {
			return Response.status(501).entity("501 Lookup Attribute Value�s is not found in the lookup table")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		if (object_id.equals("error required")) {
			return Response.status(503).entity("503 Required Category Attribute  missing")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		} else {
			long endTime = System.nanoTime();
			long timeElapsed = endTime - startTime;
			System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);
			DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now1 = LocalDateTime.now();
			String end_time = dtf1.format(now1);
			f.logs(" >> CreateDocument Request Start for " + fileDispositions.getFileName() + " started at :"
					+ start_time + " Completed at : " + end_time);
			return Response.ok(object_id).status(200).header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

	}

	@GET
	@Path("/RetrieveDocumentById")
	// @Consumes({ "multipart/form-data" })
	// @Produces({ "application/x-octet-stream" })
	@Produces(MediaType.APPLICATION_JSON)
	public Response RetrieveDocumentById(@QueryParam("doc_id") String doc_id, @QueryParam("username") String username,
			@QueryParam("password") String password, @QueryParam("version_no") String version_no) throws IOException {
		String JSONObject = "";
		if (doc_id == null || doc_id.equals("") || doc_id.equals(" ") || username == null || username.equals("")
				|| username.equals(" ") || password == null || password.equals("") || password.equals(" ")) {
			return Response.status(400).entity("400  missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);
		System.out.println("RetrieveDocumentById Request Start");
		long startTime = System.nanoTime();
		Fun f = new Fun();
		f.URL();
		byte[] arr = null;
		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		if (f.get_node_by_id(doc_id)) {
			List list = new ArrayList<>();
			if (version_no != null && version_no.length() > 0 && !version_no.equals(" ")) {
				// arr = f.get_doc_by_id(doc_id, version_no);
				list = f.get_doc_by_id2_test(doc_id, version_no);
			} else if (version_no == null || version_no.equals("") || version_no.equals(" ")) {
				String ver_no = f.get_version_no(doc_id);
				arr = f.get_doc_by_id(doc_id, ver_no);
				list = f.get_doc_by_id2_test(doc_id, ver_no);
			}

			// return Response.ok(arr, MediaType.APPLICATION_OCTET_STREAM)
			// .header("Content-Disposition", "attachment; filename=\"" + n.getName() +
			// "\"") // optional
			// .build();

			GsonBuilder gsonMapBuilder = new GsonBuilder();
			Gson gsonObject = gsonMapBuilder.create();
			JSONObject = gsonObject.toJson(list);
			long endTime = System.nanoTime();
			long timeElapsed = endTime - startTime;
			System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

			DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now1 = LocalDateTime.now();
			String end_time = dtf1.format(now1);
			f.logs(" >> RetrieveDocumentById  started at :" + start_time + " Completed at : " + end_time);

			return Response.ok(JSONObject).status(200).header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		} else {
			return Response.status(404).entity("404 Document Not Found")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
	}

	@POST
	@Path("/RetrieveDocumentBySearch")
	@Consumes({ "multipart/form-data" })
	@Produces({ "application/x-octet-stream" })
	public Response RetrieveDocumentBySearch(@FormDataParam("cat_values") String cat_values,
			@FormDataParam("cat_name") String cat_name, @FormDataParam("username") String username,
			@FormDataParam("password") String password) throws IOException, ParserConfigurationException, SAXException {

		if (cat_values == null || cat_values.equals("") || cat_values.equals(" ") || cat_name == null
				|| cat_name.equals("") || cat_name.equals(" ") || username == null || username.equals("")
				|| username.equals(" ") || password == null || password.equals("") || password.equals(" ")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);
		// System.out.println("RetrieveDocumentBySearch Request Start");
		long startTime = System.nanoTime();
		byte[] arr = null;
		Fun f = new Fun();
		f.URL();
		List<String> list = new ArrayList<String>();
		if (cat_name.contains("-")) {
			String[] arr2 = cat_name.split("-");

			for (String str : arr2)
				list.add(str);

			String token = f.authenticate(username, password);
			if (token.equals("error")) {
				return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

			List<String> cat_id = f.search_cat2(list);
			if (cat_id.contains("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

			String doc_id = f.construct_search3(cat_id, cat_values, username, password);
			if (doc_id.equals("error no item found")) {
				return Response.status(404).entity("404 document not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (!doc_id.equals("error") && !doc_id.equals("error2")) {
				arr = f.get_doc_by_id(doc_id, "0");
				long endTime = System.nanoTime();
				long timeElapsed = endTime - startTime;
				System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

				DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now1 = LocalDateTime.now();
				String end_time = dtf1.format(now1);
				f.logs(" >> RetrieveDocumentBySearch  started at :" + start_time + " Completed at : " + end_time);

				return Response.ok(arr).status(200).header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (doc_id.equals("error2")) {
				return Response.status(502).entity("502 Multiple Documents Found ")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

			else {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

		} else {

			String token = f.authenticate(username, password);
			if (token.equals("error")) {
				return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			// String ticket = f.authenticate_rest(username, password);
			// String cat_id = f.search_api_cat_test(ticket, cat_name);
			String cat_id = f.test_return(cat_name);
			if (cat_id.equals("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

			String doc_id = f.construct_search(cat_id, cat_values, username, password);
			// System.out.println("doc_id = " + doc_id);
			if (doc_id.equals("error no item found")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (!doc_id.equals("error") && !doc_id.equals("error2")) {
				// System.out.println("inside here");
				arr = f.get_doc_by_id(doc_id, "0");
				long endTime = System.nanoTime();
				long timeElapsed = endTime - startTime;
				System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

				DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now1 = LocalDateTime.now();
				String end_time = dtf1.format(now1);
				f.logs(" >> RetrieveDocumentBySearch  started at :" + start_time + " Completed at : " + end_time);

				return Response.ok(arr).status(200).header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With")
						.build();

			}
			if (doc_id.equals("error2")) {
				return Response.status(502).entity("502 Multiple Documents Found ")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

			else {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

		}

	}

	@POST
	@Path("/ImportNewVersionByID")
	@Consumes({ "multipart/form-data" })
	public Response ImportNewVersionByID(@FormDataParam("doc_id") String doc_id,
			@FormDataParam("file") InputStream input,
			@FormDataParam("file") FormDataContentDisposition fileDispositions,
			@FormDataParam("username") String username, @FormDataParam("password") String password) throws IOException {
		// System.out.println("input sent = " + input.read());
		if (input == null || doc_id == null || doc_id.equals("") || doc_id.equals(" ") || username == null
				|| username.equals("") || username.equals(" ") || password == null || password.equals("")
				|| password.equals(" ")) {

			return Response.status(400).entity("400 missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);

		// System.out.println("ImportNewVersionByID Request Start");
		long startTime = System.nanoTime();
		Fun f = new Fun();
		f.URL();
		byte[] bytes = IOUtils.toByteArray(input);
		String doc_name = fileDispositions.getFileName();

		byte ptext[] = doc_name.getBytes("ISO-8859-1");
		String value = new String(ptext, "UTF-8");

		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		// String obj_id = f.get_node(doc_id, bytes, value, username, password);
		String obj_id = f.get_node_version(doc_id, bytes, value, username, password);

		if (obj_id.equals("doc exist")) {
			return Response.status(500).entity("500 Version Uploaded successfully But Document Name Already Exist")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		if (obj_id.equals("document not found")) {
			return Response.status(404).entity("404 Document not found ")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		if (obj_id == null || obj_id.equals("") || obj_id.equals(" ")) {
			return Response.status(500).entity("500 Internal server error")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		} else {
			long endTime = System.nanoTime();
			long timeElapsed = endTime - startTime;
			System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

			DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now1 = LocalDateTime.now();
			String end_time = dtf1.format(now1);

			f.logs(" >> ImportNewVersionByID For " + fileDispositions.getFileName() + "  started at :" + start_time
					+ " Completed at : " + end_time);

			return Response.ok(obj_id).status(200).header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		}

	}

	@POST
	@Path("/ImportNewVersionBySearch")
	@Consumes({ "multipart/form-data" })
	public Response ImportNewVersionBySearch(@FormDataParam("cat_values") String cat_values,
			@FormDataParam("username") String username, @FormDataParam("password") String password,
			@FormDataParam("file") InputStream input,
			@FormDataParam("file") FormDataContentDisposition fileDispositions,
			@FormDataParam("cat_name") String cat_name) throws IOException, ParserConfigurationException, SAXException {
		if (input == null || cat_name == null || cat_name.equals("") || cat_name.equals(" ") || cat_values == null
				|| cat_values.equals("") || cat_values.equals(" ") || username == null || username.equals("")
				|| username.equals(" ") || password == null || password.equals("") || password.equals(" ")) {
			return Response.status(400).entity("400 missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);

		// System.out.println("ImportNewVersionBySearch Request Start");
		long startTime = System.nanoTime();
		Fun f = new Fun();
		f.URL();
		byte[] bytes = IOUtils.toByteArray(input);

		String doc_name = fileDispositions.getFileName();
		byte ptext[] = doc_name.getBytes("ISO-8859-1");
		String value = new String(ptext, "UTF-8");

		String obj_id = "";
		List<String> list = new ArrayList<String>();
		// Fun f = new Fun();
		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		if (cat_name.contains("-")) {
			String[] arr2 = cat_name.split("-");
			for (String str : arr2)
				list.add(str);

			List<String> cat_id = f.search_cat2(list);
			if (cat_id.contains("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

			String doc_id = f.construct_search3(cat_id, cat_values, username, password);
			if (doc_id.equals("error no item found")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (!doc_id.equals("error") && !doc_id.equals("error2")) {
				obj_id = f.get_node(doc_id, bytes, value);
				long endTime = System.nanoTime();
				long timeElapsed = endTime - startTime;
				System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);
				DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now1 = LocalDateTime.now();
				String end_time = dtf1.format(now1);

				f.logs(" >> ImportNewVersionBySearch For " + fileDispositions.getFileName() + "  started at :"
						+ start_time + " Completed at : " + end_time);
				return Response.ok(obj_id).status(200).header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (doc_id.equals("error2")) {
				return Response.status(502).entity("502 Multiple documents found ")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			} else {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
		} else {
			// String ticket = f.authenticate_rest(username, password);
			// String cat_id = f.search_api_cat_test(ticket, cat_name);
			String cat_id = f.test_return(cat_name);
			if (cat_id.equals("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			String doc_id = f.construct_search(cat_id, cat_values, username, password);
			if (doc_id.equals("error no item found")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (doc_id.equals("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

			if (doc_id.equals("error2")) {
				return Response.status(502).entity("502 Multiple documents found ")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			obj_id = f.get_node(doc_id, bytes, value);
			if (obj_id.equals("document not found")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (obj_id == null || obj_id.equals("") || obj_id.equals(" ")) {
				return Response.status(500).entity("500 Internal server error")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			} else {
				long endTime = System.nanoTime();
				long timeElapsed = endTime - startTime;
				System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

				DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now1 = LocalDateTime.now();
				String end_time = dtf1.format(now1);

				f.logs(" >> ImportNewVersionBySearch For " + fileDispositions.getFileName() + "Started at : "
						+ start_time + " Completed at : " + end_time);
				return Response.ok(obj_id).status(200).header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();

			}

		}
	}

	@POST
	@Path("/RetrieveListDocuments")
	@Consumes({ "multipart/form-data" })
	public Response RetrieveListDocuments(@FormDataParam("cat_values") String cat_values,
			@FormDataParam("username") String username, @FormDataParam("password") String password,
			@FormDataParam("cat_name") String cat_name) throws IOException, ParserConfigurationException, SAXException {

		if (cat_name == null || cat_name.equals("") || cat_name.equals(" ") || cat_values == null
				|| cat_values.equals("") || cat_values.equals(" ") || username == null || username.equals("")
				|| username.equals(" ") || password == null || password.equals("") || password.equals(" ")) {
			return Response.status(400).entity("400 missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);

		// System.out.println("RetrieveListDocuments Request Start");
		long startTime = System.nanoTime();
		Fun f = new Fun();
		f.URL();
		List<String> list = new ArrayList<String>();
		String JSONObject = "";
		HashMap<String, String> hash = new HashMap<String, String>();
		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		if (cat_name.contains("-")) {
			String[] arr2 = cat_name.split("-");

			for (String str : arr2)
				list.add(str);

			List<String> cat_id = f.search_cat2(list);
			if (cat_id.contains("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			list = f.construct_search5(cat_id, cat_values, username, password);
			if (list.contains("error no item found")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (list.size() > 0) {
				hash = f.get_names(list);
				GsonBuilder gsonMapBuilder = new GsonBuilder();
				Gson gsonObject = gsonMapBuilder.create();
				JSONObject = gsonObject.toJson(hash);
				long endTime = System.nanoTime();
				long timeElapsed = endTime - startTime;
				System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

				DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now1 = LocalDateTime.now();
				String end_time = dtf1.format(now1);

				f.logs(" >> RetrieveListDocuments  started at :" + start_time + " Completed at : " + end_time);
				return Response.ok(JSONObject).status(200).header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			} else {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}

		} else

		{
			// String cat_id = f.search_cat(cat_name);
			// String ticket = f.authenticate_rest(username, password);
			// String cat_id = f.search_api_cat_test(ticket, cat_name);
			String cat_id = f.test_return(cat_name);
			if (cat_id.equals("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			list = f.construct_search2(cat_id, cat_values, username, password);
			if (list.contains("error no item found")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			if (list.size() > 0) {
				hash = f.get_names(list);
				GsonBuilder gsonMapBuilder = new GsonBuilder();
				Gson gsonObject = gsonMapBuilder.create();
				JSONObject = gsonObject.toJson(hash);
				long endTime = System.nanoTime();
				long timeElapsed = endTime - startTime;
				System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

				DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now1 = LocalDateTime.now();
				String end_time = dtf1.format(now1);

				f.logs(" >> RetrieveListDocuments  started at :" + start_time + " Completed at : " + end_time);
				return Response.ok(JSONObject).status(200).header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();

			} else {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
		}
	}

	@POST
	@Path("/RetrieveListDocumentsByPath")
	@Consumes({ "multipart/form-data" })
	public Response RetrieveListDocumentsByPath(@FormDataParam("path") String path,
			@FormDataParam("username") String username, @FormDataParam("password") String password,
			@FormDataParam("cat_name") String cat_name) throws IOException, SQLException {

		if (path == null || path.equals("") || path.equals(" ") || username == null || username.equals("")
				|| username.equals(" ") || password == null || password.equals("") || password.equals(" ")
				|| cat_name == null || cat_name.equals("") || cat_name.equals(" ")) {
			return Response.status(400).entity("400 missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);
		// System.out.println("RetrieveListDocumentsByPath Request Start");
		long startTime = System.nanoTime();
		Fun f = new Fun();
		f.URL();
		HashMap<String, String> hash = new HashMap<String, String>();
		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		String node_id=f.check_path(path, username, password);
		//List<Node> list = f.get_nodes_under_path(path);
		List<Node> list=f.get_nodes(Long.valueOf(node_id));
		if (list == null || list.size()==0) {
			return Response.status(400).entity("400 Path Not Found").header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		if (list.size() > 0) {
			hash = f.get_names_by_path(list, cat_name);
			if (hash.containsKey("error")) {
				return Response.status(404).entity("404 Document Not found")
						.header("Access-Control-Allow-Origin", (Object) "*")
						.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With")
						.build();
			}
			String JSONObject = "";
			GsonBuilder gsonMapBuilder = new GsonBuilder();
			Gson gsonObject = gsonMapBuilder.create();
			JSONObject = gsonObject.toJson(hash);
			long endTime = System.nanoTime();
			long timeElapsed = endTime - startTime;
			System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

			DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now1 = LocalDateTime.now();
			String end_time = dtf1.format(now1);

			f.logs(" >> RetrieveListDocumentsByPath  started at :" + start_time + " Completed at : " + end_time);
			return Response.ok(JSONObject).status(200).header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		} else {
			return Response.status(404).entity("404 Document Not Found")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
	}

	@POST
	@Path("/UpdateCategory")
	@Consumes({ "multipart/form-data" })
	public Response UpdateCategory(@FormDataParam("cat_values") String cat_values,
			@FormDataParam("username") String username, @FormDataParam("password") String password,
			@FormDataParam("cat_name") String cat_name, @FormDataParam("node_id") String nodeid) throws IOException,
			DatatypeConfigurationException, ParseException, ParserConfigurationException, SAXException {

		if (username == null || username.equals("") || username.equals(" ") || password == null || password.equals("")
				|| password.equals(" ") || cat_name == null || cat_name.equals("") || cat_name.equals(" ")
				|| cat_values == null || cat_values.equals("") || cat_values.equals(" ") || nodeid == null
				|| nodeid.equals("") || nodeid.equals(" ")) {
			return Response.status(400).entity("400 missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);

		// System.out.println("UpdateCategory Request Start");
		long startTime = System.nanoTime();
		Fun f = new Fun();
		f.URL();
		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		String result = f.get_node_2(nodeid);
		if (result.equals("error")) {
			return Response.status(400).entity("400 ID Not Found").header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}

		// String cat_id = f.search_cat(cat_name);
		// String ticket = f.authenticate_rest(username, password);
		// String cat_id = f.search_api_cat_test(ticket, cat_name);
		String cat_id = f.test_return(cat_name);
		if (cat_id.equals("error")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();
		}
		String result2 = f.update_category(nodeid, cat_values, cat_id, cat_name);
		if (result2.equals("error more values")) {
			return Response.status(400).entity("400 values are more the maximum ")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();
		}
		long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;
		System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

		DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now1 = LocalDateTime.now();
		String end_time = dtf1.format(now1);

		f.logs(" >> Update Category  started at :" + start_time + " Completed at : " + end_time);
		return Response.ok("Done").status(200).header("Access-Control-Allow-Origin", (Object) "*")
				.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

	}

	@POST
	@Path("/CreateWorkspace")
	@Consumes({ "multipart/form-data" })
	public Response CreateWorkspace(@FormDataParam("WS_path") String ws_path, @FormDataParam("WS_type") String ws_type,
			@FormDataParam("WS_name") String ws_name, @FormDataParam("cat_name") String cat_name,
			@FormDataParam("cat_values") String cat_values, @FormDataParam("username") String username,
			@FormDataParam("password") String password) throws IOException, DatatypeConfigurationException,
			ParseException, ParserConfigurationException, SAXException, SQLException {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String start_time = dtf.format(now);
		// System.out.println("CreateWorkSpace request start");
		long startTime = System.nanoTime();
		if (username == null || username.equals("") || username.equals(" ") || password == null || password.equals("")
				|| password.equals(" ") || cat_name == null || cat_name.equals("") || cat_name.equals(" ")
				|| cat_values == null || cat_values.equals("") || cat_values.equals(" ") || ws_path == null
				|| ws_path.equals("") || ws_path.equals(" ") || ws_name == null || ws_name.equals("")
				|| ws_name.equals(" ")) {
			return Response.status(400).entity("400 missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

		}
		Fun f = new Fun();
		f.URL();
		String token = f.authenticate(username, password);
		if (token.equals("error")) {
			return Response.status(401).entity("401 Unauthorized (username or password incorrect)")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();
		}
		String folder_id = f.check_path(ws_path, username, password);
		// System.out.println("folder id path = " + folder_id);
		if (folder_id.equals("error") || folder_id.equals("")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();

		}

		boolean found = f.check_if_exist(folder_id, ws_name, username, password);
		if (found) {
			return Response.status(400).entity("400 Workspace Already Exist")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();

		}

		// String cat_id = f.search_cat(cat_name);
		// String ticket = f.authenticate_rest(username, password);
		// String cat_id = f.search_api_cat_test(ticket, cat_name);
		// System.out.println("cat id will be = " + cat_id);
		String cat_id = f.test_return(cat_name);
		if (cat_id.equals("error")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();
		}

		// String volume_id = f.get_volume(username, password);
		String ticket_ = f.authenticate_rest(username, password);
		// String template_id = f.get_document_template_id(ticket, ws_type);
		String template_id = f.get_template_id_xml(ws_type);
		// System.out.println("templete id = " + template_id);
		if (template_id.equals("")) {
			return Response.status(400).entity("400 Bad request - missing parameters")
					.header("Access-Control-Allow-Origin", (Object) "*")
					.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE,OPTIONS")
					.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept,X-Requested-With").build();
		}

		String result = f.createworkspacejson(folder_id, cat_name, cat_values, ws_name, template_id);
		long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;
		// System.out.println("Execution time in milliseconds : " + timeElapsed /
		// 1000000);
		String id = f.createworkspace(ticket_, result);

		DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now1 = LocalDateTime.now();
		String end_time = dtf1.format(now1);

		f.logs(" >> CreateWorkspace Started for " + ws_name + "  started at :" + start_time + " Completed at : "
				+ end_time);

		return Response.ok(id).status(200).header("Access-Control-Allow-Origin", (Object) "*")
				.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

	}

	@GET
	@Path("/CreateCategoryXml")
	public Response CreateCategoryXml(@QueryParam("username") String username, @QueryParam("password") String password)
			throws IOException, DatatypeConfigurationException, ParseException, ParserConfigurationException,
			TransformerException {
		Fun f = new Fun();
		f.URL();
		String ticket = f.authenticate_rest(username, password);

		f.CreateXml(ticket);

		return Response.status(200).header("Access-Control-Allow-Origin", (Object) "*")
				.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

	}

	@GET
	@Path("/CreateDocumentTemplateXml")
	public Response CreateDocumentTemplateXml(@QueryParam("username") String username,
			@QueryParam("password") String password) throws IOException, DatatypeConfigurationException, ParseException,
			ParserConfigurationException, TransformerException {
		Fun f = new Fun();
		f.URL();
		String ticket = f.authenticate_rest(username, password);
		f.CreateTemplateXml(ticket);

		return Response.status(200).header("Access-Control-Allow-Origin", (Object) "*")
				.header("Access-Control-Allow-Methods", (Object) "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", (Object) "Content-Type, Accept, X-Requested-With").build();

	}
}
