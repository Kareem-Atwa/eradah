package com.asset.Eradah;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class Dbclass {

	Connection con = null;
	Connection con2 = null;

	public Dbclass() {
	}

	// Fun f = new Fun();

	public void connect() throws IOException, SQLException {
		String url = "jdbc:sqlserver://localhost:1433;databaseName=opentext;user=sa;password=Asset99a";
		String id = "sa";
		String pass = "Asset99a";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = DriverManager.getConnection(url, id, pass);

		} catch (ClassNotFoundException cnfex) {

			System.out.println(cnfex.getMessage());
		}
	}

	public void connect2(String url, String user, String pass) throws IOException, SQLException {

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = DriverManager.getConnection(url, user, pass);
			System.out.println("con will be = " + con);

		} catch (ClassNotFoundException cnfex) {

			System.out.println(cnfex.getMessage());
		}

	}

	public void insert_first(HashMap<String, String> hash) throws SQLException, ClassNotFoundException, IOException {
		java.util.Date trn = get_micr_data2((String) hash.get("cheque_number"));
		LocalDate x = LocalDate.now();
		String part = x.toString();

		BufferedWriter bf = new BufferedWriter(new FileWriter("C:\\Users\\Administrator\\Desktop\\fail2.txt"));
		Connection dbconn = null;
		PreparedStatement ps = null;

		Calendar calendar = Calendar.getInstance();
		java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
		String query = " insert into first_table(bank_number,branch_number,account_number,cheque_number,cheque_type,amount,username,date,TRN_DATE) values (?,?,?,?,?,?,?,?,?)";

		try {
			connect();
			dbconn = con;
			ps = dbconn.prepareStatement(query);
			String bank_number = (String) hash.get("EQBRN_EQB_CODE");
			String branch_number = (String) hash.get("EQBRN_CODE");
			String account_number = (String) hash.get("ACCOUNT_NO");
			String cheque_number = (String) hash.get("cheque_number");
			String cheque_type = (String) hash.get("SUB_SYS_NAME");
			String amount = (String) hash.get("AMOUNT");
			String user = (String) hash.get("USER_NAME");
			ps.setString(1, bank_number);
			ps.setString(2, branch_number);
			ps.setString(3, account_number);
			ps.setString(4, cheque_number);
			ps.setString(5, cheque_type);
			ps.setString(6, amount);
			ps.setString(7, user);
			ps.setString(8, part);
			ps.setDate(9, (java.sql.Date) trn);

			ps.executeUpdate();
			System.out.println("done");
			dbconn.close();

			// f.logger("values inserted in the first table without errors");

		} catch (Exception e) {

			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
	}

	private java.util.Date get_micr_data2(String string) throws IOException, SQLException {
		connect();
		PreparedStatement ps = con.prepareStatement("SELECT * from buffer where DOCUMENT_NO=?");
		ps.setString(1, string);

		ResultSet rs = ps.executeQuery();
		java.util.Date TRN_DATE = null;
		HashMap<String, String> hash = new HashMap();
		while (rs.next()) {
			TRN_DATE = rs.getDate("TRN_DATE");
		}
		return TRN_DATE;
	}

	public List<String> get_category_data() throws IOException, SQLException {
		List<String> list = new ArrayList<String>();
		PreparedStatement ps = con2.prepareStatement("select distinct CatName from CatRegionMap");
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			list.add(rs.getString("CatName"));
		}
		return list;
	}

	public void insert_second(String micrcode_complete, String micrcode, HashMap res, String reason, HashMap hash)
			throws SQLException {
		System.out.println("----------------------------------");
		System.out.println("inside insert second");

		Connection dbconn = null;
		PreparedStatement ps = null;
		LocalDate date = LocalDate.now();
		String dat = date.toString();
		Calendar calendar = Calendar.getInstance();
		java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
		System.out.println(startDate);
		String query = " INSERT INTO second_table(bank_number,branch_number,account_number,cheque_number,date,reason,TRN_DATE) values (?,?,?,?,?,?,?)";

		try {
			connect();
			dbconn = con;
			System.out.println("");
			ps = dbconn.prepareStatement(query);
			String bank_no = (String) res.get("BANK_NUMBER");
			if (bank_no == null) {
				bank_no = "1";
			}
			String acc_no = (String) res.get("ACCOUNT_NO");
			String bran_no = (String) res.get("BRANCH_NO");
			String doc_no = (String) res.get("DOCUMENT_NO");
			String datee = (String) hash.get("TRN_DATE");
			if (datee.equals("null")) {
				datee = null;
			}
			System.out.println("1 - " + bank_no);
			System.out.println("2 - " + acc_no);
			System.out.println("3 - " + bran_no);
			System.out.println("4 - " + doc_no);
			System.out.println("5 - " + dat);
			System.out.println("6 - " + reason);
			System.out.println("7 - " + datee);

			System.out.println("bank number = " + res.get("BANK_NUMBER"));
			System.out.println("account number = " + res.get("ACCOUNT_NO"));
			System.out.println("branch number = " + res.get("BRANCH_NO"));
			System.out.println("document number = " + res.get("DOCUMENT_NO"));

			ps.setString(1, bank_no);
			ps.setString(2, bran_no);
			ps.setString(3, acc_no);
			ps.setString(4, doc_no);
			ps.setString(5, dat);
			ps.setString(6, reason);
			ps.setString(7, datee);
			ps.executeUpdate();
			res.clear();

		} catch (Exception e) {

			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
	}

	public String check_date(String date) throws SQLException, IOException {
		connect();

		PreparedStatement ps = con.prepareStatement("SELECT * from buffer where TRN_DATE=?");
		ps.setString(1, date);

		ResultSet rt = ps.executeQuery();

		if (!rt.isBeforeFirst()) {
			return "notfound";
		}
		return "found";
	}

	public HashMap<String, String> get_micr_data(String cheque_no) throws SQLException, IOException {
		connect();
		Fun f = new Fun();
		// get all micr data from database .
		LocalDate x = LocalDate.now();
		String part = x.toString();
		System.out.println("param passed =" + cheque_no);
		PreparedStatement ps = con.prepareStatement("SELECT * from buffer where DOCUMENT_NO=?");
		PreparedStatement ps2 = con.prepareStatement("SELECT * from buffer where DOCUMENT_NO=?");
		PreparedStatement ps4 = con.prepareStatement("SELECT * from buffer where DOCUMENT_NO=?");

		PreparedStatement ps3 = con.prepareStatement("SELECT * from buffer where DOCUMENT_NO=? and SCANNED=?");
		ps.setString(1, cheque_no);
		ps2.setString(1, cheque_no);
		ps3.setString(1, cheque_no);
		ps3.setInt(2, 1);
		ps4.setString(1, cheque_no);
		ResultSet rs = ps.executeQuery();
		ResultSet rs2 = ps2.executeQuery();
		ResultSet rs3 = ps3.executeQuery();
		ResultSet rs4 = ps4.executeQuery();
		int count = 0;
		HashMap<String, String> hash = new HashMap();
		int counter = 0;
		while (rs3.next()) {

			count++;
		}

		if (count == 1) {
			hash.put("scanned_code", "300");
			// f.logger("get data fun ->>> status code 300 cheque has been scanned befor");
			return hash;
		}
		while (rs.next()) {
			counter++;
		}

		if (counter == 0) {

			hash.put("status_code", "404");
			hash.put("TRN_DATE", "null");
			// f.logger("get data fun ->>> status code 404 cheque data not found");
			return hash;
		}
		if (counter > 1) {
			hash.put("status_code", "500");
			rs4.next();
			java.util.Date TRN_DATE = rs4.getDate("TRN_DATE");
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String TRN_DATE_2 = dateFormat.format(TRN_DATE);
			// f.logger("get data fun ->>> status code 500 more than one record found");
			hash.put("TRN_DATE", TRN_DATE_2);

			return hash;
		}
		if (counter == 1) {
			hash.put("status_code", "0");
			while (rs2.next()) {
				String bank_number = rs2.getString("EQBRN_EQB_CODE");
				String branch_number = rs2.getString("EQBRN_CODE");
				String SUB_SYS_NAME = rs2.getString("SUB_SYS_NAME");
				int SUB_SYS_CODE = rs2.getInt("SUB_SYS_CODE");
				String SUB_SYS_CODE_2 = Integer.toString(SUB_SYS_CODE);
				java.util.Date TRN_DATE = rs2.getDate("TRN_DATE");
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String TRN_DATE_2 = dateFormat.format(TRN_DATE);
				Long ACCOUNT_NO = Long.valueOf(rs2.getLong("ACCOUNT_NO"));
				String ACCOUNT_NO_2 = Long.toString(ACCOUNT_NO.longValue());
				Double AMOUNT = Double.valueOf(rs2.getDouble("AMOUNT"));
				String AMOUNT_2 = Double.toString(AMOUNT.doubleValue());
				Long cheque_number = Long.valueOf(rs2.getLong("DOCUMENT_NO"));
				String cheque_number_2 = Long.toString(cheque_number.longValue());
				java.util.Date DOCUMENT_DATE = rs2.getDate("DOCUMENT_DATE");
				DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
				String DOCUMENT_DATE_2 = dateFormat2.format(DOCUMENT_DATE);
				String USER_NAME = rs2.getString("USER_NAME");
				int VOUCHER_NO = rs2.getInt("VOUCHER_NO");
				String VOUCHER_NO_2 = Integer.toString(VOUCHER_NO);
				int CURRENCY_ID = rs2.getInt("CURRENCY_ID");
				String CURRENCY_ID_2 = Integer.toString(CURRENCY_ID);
				String CURRENCY_NAME = rs2.getString("CURRENCY_NAME");
				hash.put("EQBRN_EQB_CODE", bank_number);
				hash.put("EQBRN_CODE", branch_number);
				hash.put("SUB_SYS_NAME", SUB_SYS_NAME);
				hash.put("SUB_SYS_CODE", SUB_SYS_CODE_2);
				hash.put("TRN_DATE", TRN_DATE_2);
				hash.put("ACCOUNT_NO", ACCOUNT_NO_2);
				hash.put("AMOUNT", AMOUNT_2);
				hash.put("cheque_number", cheque_number_2);
				hash.put("DOCUMENT_DATE", DOCUMENT_DATE_2);
				hash.put("USER_NAME", USER_NAME);
				hash.put("VOUCHER_NO", VOUCHER_NO_2);
				hash.put("CURRENCY_ID", CURRENCY_ID_2);
				hash.put("CURRENCY_NAME", CURRENCY_NAME);

				// System.out.println("inside get hash loop");
				// System.out.println((String) hash.get("EQBRN_EQB_CODE"));
				// System.out.println((String) hash.get("EQBRN_CODE"));
				// System.out.println((String) hash.get("SUB_SYS_NAME"));
				// System.out.println((String) hash.get("SUB_SYS_CODE"));
				// System.out.println((String) hash.get("TRN_DATE"));
				// System.out.println((String) hash.get("ACCOUNT_NO"));
				// System.out.println((String) hash.get("AMOUNT"));
				// System.out.println((String) hash.get("cheque_number"));
				// System.out.println((String) hash.get("DOCUMENT_DATE"));
				// System.out.println((String) hash.get("USER_NAME"));
				// System.out.println((String) hash.get("VOUCHER_NO"));
				// System.out.println((String) hash.get("CURRENCY_ID"));
				// System.out.println((String) hash.get("CURRENCY_NAME"));

				// f.logger("get data fun ->>> status code 0 (200) data found and the values are
				// : \n EQBRN_EQB_CODE = "
				// + hash.get("EQBRN_EQB_CODE") + " EQBRN_CODE = " + hash.get("EQBRN_CODE") + "
				// SUB_SYS_NAME = "
				// + hash.get("SUB_SYS_NAME") + " SUB_SYS_CODE = " + hash.get("SUB_SYS_CODE") +
				// " TRN_DATE = "
				// + hash.get("TRN_DATE") + " ACCOUNT_NO = " + hash.get("ACCOUNT_NO") + " AMOUNT
				// = "
				// + hash.get("AMOUNT") + " cheque_number = " + hash.get("cheque_number") + "
				// DOCUMENT_DATE = "
				// + hash.get("DOCUMENT_DATE") + " USER_NAME = " + hash.get("USER_NAME") + "
				// VOUCHER_NO = "
				// + hash.get("VOUCHER_NO") + "CURRENCY_ID = " + hash.get("CURRENCY_ID") + "
				// CURRENCY_NAME = "
				// + hash.get("CURRENCY_NAME"));
			}
		}
		return hash;
	}

	public void flag(String cheque_number) throws IOException, SQLException {
		Long x = Long.valueOf(Long.parseLong(cheque_number));
		connect();
		Connection dbconn = null;
		dbconn = con;
		String query = "UPDATE [dbo].[buffer] SET SCANNED = ? where DOCUMENT_NO = ?";

		PreparedStatement preparedStmt = dbconn.prepareStatement(query);
		preparedStmt.setInt(1, 1);
		preparedStmt.setLong(2, x.longValue());
		preparedStmt.executeUpdate();

		dbconn.close();

		// f.logger("flag is set without errors");
	}

	public HashMap<String, String> get_category_data(String cat_name) throws SQLException {
		System.out.println("cat_name = " + cat_name);

		List<String> list = new ArrayList<String>();
		HashMap<String, String> hash = new HashMap<String, String>();
		PreparedStatement ps = con
				.prepareStatement("select AttrName,AttrType  from [csadmin].[CatRegionMap] where CatName=?");
		ps.setString(1, cat_name);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			System.out.println(rs.getString("AttrName") + "  " + rs.getString("AttrType"));
			hash.put(rs.getString("AttrName"), rs.getString("AttrType"));

		}
		return hash;

	}

	public List<String> load_data() {
		List<String> list = new ArrayList<String>();
		String url = "", username = "", password = "";
		Properties prop = new Properties();
		InputStream input = null;
		String result = "";

		try {
			String currentRelativePath = System.getProperty("catalina.base");
			input = new FileInputStream(currentRelativePath + "/config.properties");

			System.out.println("full path will be = " + currentRelativePath + "/config.properties");
			// load a properties file
			prop.load(input);
			// get the property value and print it out
			list.add(prop.getProperty("url"));
			list.add(prop.getProperty("username"));
			list.add(prop.getProperty("password"));
			// url = prop.getProperty("url");
			// username = prop.getProperty("username");
			// password = prop.getProperty("password");
			System.out.println("url =" + list.get(0));
			System.out.println("username = " + list.get(1));
			System.out.println("password = " + list.get(2));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;

	}

	public List<String> execute_sql(String sql) throws SQLException {
		System.out.println("sql inside execute function  = " + sql);
		List<String> list = new ArrayList<String>();
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			list.add(rs.getString(1));
		}

		return list;
	}

	public String get_path(String id, String string) throws IOException, SQLException {

		String result = "";
		connect();

		PreparedStatement ps = con
				.prepareStatement("select DataID  from [opentext].[dbo].[DTreeCore] where Name=? and ParentID=?");
		ps.setString(1, string);
		ps.setInt(2, Integer.parseInt(id));
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			System.out.println("id returned = " + rs.getString("DataID"));
			result = rs.getString("DataID");
			return result;

		}
		return result;

	}

}
